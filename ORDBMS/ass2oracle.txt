--Route (Route_no int, Source char(20), Destination char(20),no_of_stations  int)

create type route1 as object(Route_no int,Source char(20),Destination char(20),no_of_station int);

create table route of route1(Route_no primary key);

insert into route values(11,'Swargate','Hadapsar',5);
insert into route values(12,'Swargate','Hadapsar',7);
insert into route values(13,'Swargate','Hadapsar',9);
insert into route values(17,'Swargate','Hadapsar',2);
insert into route values(14,'Chichwad','Corporation',5);
insert into route values(15,'Chichwad','Corporation',9);
insert into route values(16,'Chichwad','Corporation',2);
insert into route values(18,'Hinjawadi','Corporation',20);


-------------------------------------
 --Driver (driver_no int , driver_name char(20), license_no  int, address char(20), d_age int , salary  float)The 
 
 create type driver1 as object(driver_no int,driver_name char(20),license_no int,address char(20),d_age int,salary float);
 
 create table driver of driver1(driver_no primary key,license_no unique);
 
 insert into driver values(101,'Raj',19990,'Pune',35,12000);
 insert into driver values(102,'Rajnish',19991,'Hinjawadi',36,13000);
 insert into driver values(103,'Kamalesh',19992,'Corporation',32,11000);
 insert into driver values(104,'Talib',19993,'CCL',38,13000);
 insert into driver values(105,'Pratik',19994,'Mumbai',31,12000);
 ------------------------------------------
--  Bus (Bus_noint ,  Capacity int ,  depot_name varchar(20))     **bus - route M-1
 
 create type bus1 as object(Bus_no int,Capacity int,depot_name varchar2(20), 
 route2 ref route1);
 
 create table bus of bus1(Bus_no primary key,Capacity NOT NULL);
 
 insert into bus select 1,20,'Swargate',ref(r) from route r where r.Route_no = 11;
 insert into bus select 2,30,'Swargate',ref(r) from route r where r.Route_no = 12;
 insert into bus select 3,20,'Swargate',ref(r) from route r where r.Route_no = 13;
 insert into bus select 4,10,'Chichwad',ref(r) from route r where r.Route_no = 14;
 insert into bus select 5,10,'Hinjawadi',ref(r) from route r where r.Route_no = 18;
 insert into bus select 6,10,'Chichwad',ref(r) from route r where r.Route_no = 15; --new record
 
 
 
 -------------------------------------------------------
--  Bus_Driver : M-M with descriptive  attributes Date of duty alloted and Shift –it can be 1 (Morning) 0r 2 ( Evening ).
 
create type Bus_Driver1 as object(dateOfDuty date, shift int,bus2 ref bus1,driver2 ref driver1);
 create table Bus_Driver of Bus_Driver1(shift check(shift = 1 or shift = 2));
 
 drop table Bus_Driver;
 drop type Bus_Driver1;
 
insert into Bus_Driver select '12-jan-2008','1',ref(b),ref(dd) from bus b,driver dd where
b.Bus_no=1 and dd.driver_no=101;

insert into Bus_Driver select '12-jan-2008','2',ref(b),ref(dd) from bus b,driver dd where
b.Bus_no=1 and dd.driver_no=101; --new record

insert into Bus_Driver select '12-jan-2016','1',ref(b),ref(dd) from bus b,driver dd where
b.Bus_no=2and dd.driver_no=102;

insert into Bus_Driver select '12-jan-2008','1',ref(b),ref(dd) from bus b,driver dd where
b.Bus_no=3 and dd.driver_no=103;

insert into Bus_Driver select '12-jan-2020','2',ref(b),ref(dd) from bus b,driver dd where
b.Bus_no=4 and dd.driver_no=104;

insert into Bus_Driver select '12-jan-2020','2',ref(b),ref(dd) from bus b,driver dd where
b.Bus_no=5 and dd.driver_no=105;

 
 --------------------------------------QUERY-------------------------
1:Find out the drivers working in shift 1.

 select deref(driver2).driver_name from Bus_Driver where shift = 1;

DEREF(DRIVER2).DRIVER_NAME
Raj                 
Kamalesh            
Rajnish      

-----------------

2:Find out the route details on which buses of capacity 20 runs.

select route.Route_no,route.Source,route.Destination,route.no_of_station from route,bus
where route.Route_no = deref(route2).Route_no and Capacity = 20;

ROUTE_NO	SOURCE	DESTINATION	NO_OF_STATION
11	Swargate            	Hadapsar            	5
13	Swargate            	Hadapsar            	9

-----------------

3:Find the names and  their license no.  of drivers working on 12-01-2008 in both the shifts.

select driver.driver_name,driver.license_no from driver where driver_no in ( 
select deref(driver2).driver_no from Bus_Driver where dateOfDuty = '12-jan-2008' and shift = 1 and deref(driver2).driver_no in (
select deref(driver2).driver_no from Bus_Driver where dateOfDuty = '12-jan-2008' and shift = 2))

DRIVER_NAME	        LICENSE_NO
Raj                 	19990
------------------

4:Delete all the routes where number of stations are less than 3.

select * from route;

ROUTE_NO	SOURCE	                DESTINATION	    NO_OF_STATION
11	        Swargate            	Hadapsar            	5
12	        Swargate            	Hadapsar            	7
13	        Swargate            	Hadapsar            	9
14	        Chichwad            	Corporation         	5
15	        Chichwad            	Corporation         	9
18	        Hinjawadi           	Corporation         	20
17	        Swargate            	Hadapsar            	2
16	        Chichwad            	Corporation         	2

delete from route where no_of_station < 3;

select * from route;

ROUTE_NO	SOURCE	                DESTINATION	    NO_OF_STATION
11	        Swargate            	Hadapsar            	5
12	        Swargate            	Hadapsar            	7
13	        Swargate            	Hadapsar            	9
14	        Chichwad            	Corporation         	5
15	        Chichwad            	Corporation         	9
18	        Hinjawadi           	Corporation         	20

---------------------------


6:Find out the number of buses running from 'Chichwad' to ‘Corporation’.

select count(Bus_no) from bus,route where route.Route_no = deref(route2).Route_no and route.Source = 'Chichwad' and route.Destination = 'Corporation';

COUNT(BUS_NO)
2

----------------

 7:Update the salary of driver by 1000 if  his age > 35 

 select * from driver;

 DRIVER_NO	DRIVER_NAME	            LICENSE_NO	ADDRESS	                D_AGE	   SALARY
101	        Raj                 	19990	    Pune                	35	        12000
102	        Rajnish             	19991	    Hinjawadi           	36	        13000
103	        Kamalesh            	19992	    Corporation         	32	        11000
104	        Talib               	19993	    CCL                 	38	        13000
105	        Pratik              	19994	    Mumbai              	31	        12000


update driver set salary = salary+1000 where d_age > 35

select * from driver;

DRIVER_NO	DRIVER_NAME	            LICENSE_NO	ADDRESS             	D_AGE	SALARY
101	        Raj                 	19990	    Pune                	35	    12000
102	        Rajnish             	19991	    Hinjawadi           	36	    14000
103	        Kamalesh            	19992	    Corporation         	32	    11000
104	        Talib               	19993	    CCL                 	38	    14000
105	        Pratik              	19994	    Mumbai              	31	    12000

-----------------------
8:List the bus numbers which are running  from ‘Swargate’ to ‘Hadapsar’ having bus capacity 5

 select bus.Bus_no from bus,route where route.Route_no = deref(route2).Route_no and route.Source = 'Swargate' and route.Destination = 'Hadapsar' and bus.Capacity = 20; 

 BUS_NO
    1
    3
-==================================================================================================================================================================================

Client-Policy Database
***********************

create type policy1 as object(policy_name varchar2(20) , min_age_limit integer ,
max_age_limit integer , maturity_age integer , min_sum_assured integer , 
max_sum_assured integer);

create table policy of policy1(policy_name primary key);
drop table policy;

insert into policy values('Life Policy',20,60,10,40000,80000);
insert into policy values('Health Policy',20,45,15,500000,700000);
insert into policy values('Travel Policy',23,60,10,50000,70000);
insert into policy values('Motor Policy',18,60,20,70000,80000);
insert into policy values('Property Policy',25,60,20,700000,800000);
insert into policy values('j-a policy',25,60,20,700000,800000);
insert into policy values('j-b policy',20,60,10,50000,89000);



------------------------------------------------------------------------------
create type client1 as object(client_id integer , name varchar(25) , 
birth_date date ,nominee_name varchar(25) ,relation_with_client  varchar(20));

create table client of client1(client_id primary key); 


insert into client values(101,'Khushi Singh','25-jun-1996','Anand Singh','Brother');
insert into client values(102,'Pooja Singh','08-nov-1990','Azad Singh','Brother');
insert into client values(103,'Virat Singh','21-dec-1991','BN Singh','Nephew');
insert into client values(104,'Ayushi Rai','30-aug-1991','Heena Singh','Friend');
insert into client values(105,'Jhanavi Rai','26-jan-1990','Arti Singh','Friend');
insert into client values(106,'Yash Negi','26-jun-1990','Arti Singh','Friend');

-------------------------------------------------------------
create type agent1 as object(agent_id integer  , name varchar(25), license_no integer , branch_office varchar2(20));

create table agent of agent1(agent_id primary key);
drop table agent;

insert into agent values(201,'Srot Singhal',10001,'Jiyanpur');
insert into agent values(202,'Harshita Thakur',10002,'Varansi');
insert into agent values(203,'Aishwarya Sakunde',10003,'Hinjawadi');
insert into agent values(204,'Mansi Mandhare',10004,'Mumbai-1');
insert into agent values(205,'Shruti Purandare',10005,'Mumbai-1');
insert into agent values(206,'Pradeep Patil',10006,'Pune');

--------------------------------------------------
create type Agent_client_policy1 as object(policy_no integer , premium decimal(7,2) , policy_date date ,
type_premium varchar(20) , sum_assured decimal(7,2) ,
term integer,policy2 ref policy1,client2 ref client1,agent2 ref agent1);

create table Agent_client_policy of Agent_client_policy1(policy_no UNIQUE,
type_premium check(type_premium = 'q' or type_premium = 'h' or type_premium = 'y'));


insert into Agent_client_policy select 2111,30000.0,'1-mar-2003','q',40000.0,5,ref(p),ref(c),ref(a) from
policy p,client c,agent a where p.policy_name = 'j-a policy'and c.client_id = 101 and a.agent_id = 206;

insert into Agent_client_policy select 2112,35000.0,'1-mar-2004','q',45000.0,5,ref(p),ref(c),ref(a) from
policy p,client c,agent a where p.policy_name = 'j-b policy'and c.client_id = 102 and a.agent_id = 206;

insert into Agent_client_policy select 2113,20000.0,'3-jun-2003','h',30000.0,7,ref(p),ref(c),ref(a) from
policy p,client c,agent a where p.policy_name = 'j-a policy'and c.client_id = 103 and a.agent_id = 205;

insert into Agent_client_policy select 2114,30000.0,'9-sep-2000','y',40000.0,5,ref(p),ref(c),ref(a) from
policy p,client c,agent a where p.policy_name = 'j-b policy'and c.client_id = 104 and a.agent_id = 204;

insert into Agent_client_policy select 2115,44000.0,'8-jan-2005','q',20000.0,4,ref(p),ref(c),ref(a) from
policy p,client c,agent a where p.policy_name = 'Life Policy'and c.client_id = 101 and a.agent_id = 203;

insert into Agent_client_policy select 2116,24000.0,'8-aug-2005','y',20000.0,4,ref(p),ref(c),ref(a) from
policy p,client c,agent a where p.policy_name = 'Health Policy'and c.client_id = 105 and a.agent_id = 201;

insert into Agent_client_policy select 2117,35000.0,'10-mar-2004','h',45000.0,5,ref(p),ref(c),ref(a) from
policy p,client c,agent a where p.policy_name = 'j-b policy'and c.client_id = 102 and a.agent_id = 206;

-----------------------------------------

1. Count the number of clients who have taken policies from branch office 'Pune' 

select count(client_id) from client,agent,Agent_client_policy where
client.client_id = deref(client2).client_id and agent.agent_id = deref(agent2).agent_id
and agent.branch_office = 'Pune';

COUNT(CLIENT_ID)
2

------------
2 Give the name of agent having maximum countof clients.

select a.name,count(c.name) from client c,agent a,Agent_client_policy where
c.client_id = deref(client2).client_id and
a.agent_id = deref(agent2).agent_id group by a.name 
order by count(c.name) desc FETCH FIRST ROW ONLY;


NAME	COUNT(C.NAME)
Mansi Mandhare	4

------------------

3 Find the name of clients who have taken j-b policy and premeum if hals yearly on 1st march, (  year does not matter .)

select c.name from client c,policy p,Agent_client_policy where 
c.client_id = deref(client2).client_id and
p.policy_name = deref(policy2).policy_name and
p.policy_name = 'j-b policy' and type_premium = 'h';


NAME
Pooja Singh


----------------

4 Count the number of clients of ‘j-a’ policies from ‘mumbai-1’ branch office

select count(c.name) from client c,agent a,policy p,Agent_client_policy where
 c.client_id = deref(client2).client_id and
 a.agent_id = deref(agent2).agent_id and
 p.policy_name = deref(policy2).policy_name and
 p.policy_name = 'j-a policy' and a.branch_office = 'Mumbai-1';


 COUNT(C.NAME)
    1


------------------
5 Find the total premium amount of client ‘________’.

select sum(premium) from Agent_client_policy,client where 
client.client_id = deref(client2).client_id and client.client_id = 101;

SUM(PREMIUM)
74000

=========================================================**END**==================================