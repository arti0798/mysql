PARALLER DATABASE
*******************

1. Define Scale up, Speed up in Parallel Database.

    Speedup: Speedup is defined as the ratio between the runtime with one processor and 
        the runtime using multiple processors. It measures the performance improvement gained 
        using multiple processors instead of a single processor and 
        is calculated using formula:

    Speedup = Time1 / TimeM

    Time1 => Time1 is the time it takes to execute a task using only one processor.
    TimeM => TimeM is the time it takes to execute that same task using M processors.

    Scaleup : Scaleup is the ability of an application to retain response time as the job size or 
        the transaction volume increases by adding additional processors and disks.
        Scaleup is calculated using the following formula:

    Scaleup = Volumem / Volume1

    In database applications, scaleup can be either batch or transactional.
    Batch scaleup : Batch scaleup, larger batch jobs can be supported without a loss of response time.
    Transaction scaleup: Transaction scaleup, larger numbers of transactions can be supported without 
        loss of response time. 

    In both cases, response time is maintained by the addition of more processors.

2. Goals of paraller DB.

    a. Improve performance: The performance of the system can be improved by connecting multiple CPU and 
        disks in parallel. Many small processors can also be connected in parallel.

    b. Improve availability of data: Data can be copied to multiple locations to improve the availability of data.

    c. Improve reliability: Reliability of system is improved with completeness, accuracy and availability of data.
    
    d. Provide distributed access of data: Companies having many branches in multiple cities can access data with the 
        help of parallel database system.

3. Expalin the architecture of paraller DB.

    In parallel database architecture, there are multiple central processing units (CPUs) 
    connected to a computer system. There are several architectural models for parallel machines. 
    Three of the most prominent ones are listed below:

    Shared-memory multiple CPU.

    Shared-disk multiple CPU.

    Shared-nothing multiple CPU.

4. Shared memory system, Shared disk system, Shared nothing system.

    Shared memory system : In Shared Memory architecture, single memory is shared among many processors.
        several processors are connected through an interconnection network with Main memory and disk setup. 
        Here interconnection network is usually a high speed network (may be Bus, Mesh, or Hypercube) which 
        makes data sharing (transporting) easy among the various components (Processor, Memory, and Disk).

         Advantages: 
         a. Simple implementation
         b. Establishes effective communication between processors through single memory addresses space.
         c. Less communication overhead

         Disadvantage: 
         a. It leads to bottleneck problem 
         b. Expensive to build 
         c. It is less sensitive to partitioning
         d. Addition of processor would slow down the existing processors.

    Shared disk system :  In Shared Disk architecture, single disk or single disk setup is shared among 
        all the available processors and also all the processors have their own private memories.

        Advantages:
        a. Failure of any processors would not stop the entire system (Fault tolerance).
        b. Interconnection to the memory is not a bottleneck. (It was bottleneck in Shared Memory architecture).
        c. Support larger number of processors (when compared to Shared Memory architecture).

        Disadvantage:
        a. Interconnection to the disk is bottleneck as all processors share common disk setup.
        b. Inter-processor communication is slow. The reason is, all the processors have their own memory.
         Hence, the communication between processors need reading of data from other processors’ 
         memory which needs additional software support.
         EG :  DEC clusters (VMScluster)  running Rdb

    Shared nothing system:  In Shared Nothing architecture, every processor has its own memory and disk setup. 
        This setup may be considered as set of individual computers connected through high speed 
        interconnection network using regular network protocols and switches 
        for example to share data between computers. 
        (This architecture is used in the Distributed Database System). 
        In Shared Nothing parallel database system implementation, we insist the use of similar nodes that are Homogenous systems. 
        (In distributed database System we may use Heterogeneous nodes). 

    Advantages: 
    a. Number of processors used here is scalable. 
        That is, the design is flexible to add more number of computers.

    b. Unlike in other two architectures, only the data request 
        which cannot be answered by local processors need to be forwarded through interconnection network.

    Disadvantages:
    a. Non-local disk accesses are costly. That is, if one server receives the request.
    b. Communication cost involved in transporting data among computers.
       
    EG: Oracle nCUBE.

5. Explain various methods of achieving parallelism in Quries.

    Parallel query processing is the major solution to high-performance management of very large databases. 
    The basic principle is to partition the database across multiple disks or memory nodes so that much inter- and intra-query parallelism can be gained. 

I/O Parallelism 
•	Reduce the time required to retrieve relations from disk by partitioning
•	The relation on multiple disk.

Inter query Parallelism 
•	Queries/transactions execute in parallel with one another. 
•	Increases transaction throughput; used primarily to scale up a transaction processing system to support a larger number of transactions per second. 
•	Easiest form of parallelism to support, particularly in a shared memory parallel database, because even sequential database systems support concurrent processing.
•	More complicated to implement on shared-disk or shared-nothing architectures 

Intra query Parallelism 
•	Execution of a single query in parallel on multiple processors/disks, important for speeding up long-running queries.   
Intra operation Parallelism  
•	Parallelize the execution of each individual operation in the query. 
Interoperation Parallelism  
•	Execute the different operations in a query expression in parallel.    

    

    





