                                                        MONGODB
                                                        --------

1: Explain the features of MONGODB.

    a. Schema-less Database: A Schema-less database means one collection can hold different types of documents in it.
    It is not necessary that the one document is similar to another document like in the relational databases. 

    b. Document Oriented: In MongoDB, all the data stored in the documents instead of tables like in RDBMS.
    In these documents, the data is stored in fields(key-value pair) instead of rows and columns which make 
    the data much more flexible in comparison to RDBMS. 
    And each document contains its unique object id.

    c. Indexing: In MongoDB database, every field in the documents is indexed with primary and secondary indices
    this makes easier and takes less time to get or search data from the pool of the data. 

    d. Scalabiltiy: MongoDB provides horizontal scalability with the help of sharding. 
    Sharding means to distribute data on multiple servers, here a large amount of data is partitioned into 
    data chunks using the shard key, and these data chunks are evenly distributed across shards that reside 
    across many physical servers. It will also add new machines to a running database.

    e. Replication: MongoDB provides high availability and redundancy with the help of replication, 
    it creates multiple copies of the data and sends these copies to a different server so that if one server fails, 
    then the data is retrieved from another server.

    f. Aggregation: It allows to perform operations on the grouped data and get a single result or computed result.
    It provides three different aggregations i.e, aggregation pipeline, map-reduce function, and single-purpose aggregation methods.

    g. High Performance: The performance of MongoDB is very high and data persistence as compared to another database due to its features 
    like scalability, indexing, replication, etc.


2. Explain CRUD in MongoDB with syntax & suitable example.

    MongoDB CURD Operations:
    CURD is Create Update, Read and Delete.

    a. Create Operations : Create or insert operations add new documents to a collection. If the collection does not currently exist, 
    insert operations will create the collection.

    Eg: db.collection.insert();

        db.Film.insert({"Film Id":100,"Title of Film":"Little Fires","Year of Release":2010,"genre":["drama","adventure"],
        "actors":[{"fname":"Lina","lname":"Duke"},{"fname":"Emma","lname":"watson"}],"directors":[{"fname":"Jhone","lname":"watson"},{"fname":"Steve","lname":"Smith"}],"release details":[{"places of release":["Newyok","Canada"]},{"dates":["1/1/2010","2/2/2010"]},{"rating of film":4}]})

    b. Update Operations : Update operations modify existing documents in a collection. MongoDB provides the following methods to update documents of a collection:

        db.collection.replaceOne() 
        db.collection.updateMany()
        db.collection.updateOne()

    c. Read Operations : Read operations retrieve documents from a collection; i.e. query a collection for documents. 
    MongoDB provides the following methods to read documents from a collection:

    Eg: db.collection.find()
        db.Film.find();
     
    d. Delete Operations : Delete operations remove documents from a collection. MongoDB provides the following methods to delete documents of a collection:

    Eg : db.collection.deleteOne()
         db.collection.deleteMany() 

3. Explain sorting in MongoDB with suitable example.  

    In MongoDB, sorting is done by the sort() method. The sort() method consists of two basic building blocks. 
    These building blocks are fields to be sorted and the sort order.
    The sorting order in MongoDB is defined by either a one (1) or a minus (-1). 
    Here the positive one represents the ascending order, while the negative one represents the descending order.

    Eg: db.collection_name.find().sort({field_name: sort order})
        db.Film.find().sort({"Title of Film":1,"Year of Release":-1}).pretty()

4. Explain skip() and limit() in MongoDB with suitable example.    

    The limit() function in MongoDB is used to specify the maximum number of results to be returned. 
    Only one parameter is required for this function to return the number of the desired result.

    Eg : > db.Film.find({"actors.fname":"Madhuri"},{_id:0,"Title of Film":1}).sort({_id:-1}).limit(2)


    The skip() method is used for skipping the given number of documents in the Query result.

    Eg: > db.Film.find({"actors.fname":"Madhuri"},{_id:0,"Title of Film":1}).sort({_id:-1}).limit(2).skip(1);

5. Explain Aggregation in MONGODB with syntax & example.  
    
    Aggregation basically groups the data from multiple documents and operates in many ways on those grouped data 
    in order to return one combined result.

    Eg: db.collection_name.aggregate(aggregate_operation);

    Different expressions used by Aggregate function : 

    $sum : Summates the defined values from all the documents in a collection.
    $avg : Calculates the average values from all the documents in a collection.
    $min : Return the minimum of all values of documents in a collection.
    $max : Return the maximum of all values of documents in a collection.
    $push : Inserts values to an array in the resulting document.
    $first : Returns the first document from the source document.
    $last : Returns the last document from the source document.


6. How regex can be used in MongoDB?

    Provides regular expression capabilities for pattern matching strings in queries.
    MongoDB uses PCRE(Perl compatible regular expressions).

    The following <options> are available for use with regular expression.
    a)   i: Case insensitivity to match upper and lower cases.
        Eg : db.customer.find( { name: { $regex: /^ABC/i } } )

    b)   m: For patterns that include anchors (i.e. ^ for the start, $ for the end),
    match at the beginning or end of each line for strings with multiline values. 

        Eg : db.customer.find( { item: { $regex: /^S/, $options: 'm' } } )   

    c)  x: “Extended” capability to ignore all white space characters in the $regex pattern 
    unless escaped or included in a character class.   

7. Explain the usage of Indexing in MONGODB.

    An index in MongoDB is a special data structure that holds the data of few fields of 
    documents on which the index is created. Indexes improve the speed of search operations in database 
    because instead of searching the whole document, the search is performed on the indexes that holds only few .
    syntax:
        db.collection_name.createIndex({field_name: 1 or -1})

    getIndexes() method to find all the indexes created on a collection. 
    syntax:
        db.collection_name.getIndexes()    

    dropIndex() method is used for dropping the index.    
    syntax:
        db.collection_name.dropIndex({index_name: 1})

8. How cursor can be used in MongoDB?

    The Cursor is a MongoDB Collection of the document which is returned upon the find method execution.
    By default, it is automatically executed as a loop.

    Cursor Method:

    cursor.count() : Modifies the cursor to return the number of documents in the result set rather than the documents themselves.
    cursor.forEach() : Applies a JavaScript function for every document in a cursor.
    cursor.hasNext() : Returns true if the cursor has documents and can be iterated.
    cursor.limit() : Constrains the size of a cursor’s result set.
    cursor.map() : Applies a function to each document in a cursor and collects the return values in an array.
    cursor.max() : Specifies an exclusive upper index bound for a cursor.
    cursor.next() : Returns the next document in a cursor.
    cursor.pretty() : Configures the cursor to display results in an easy-to-read format.
    cursor.skip() : Returns a cursor that begins returning results only after passing or skipping a number of documents.
    cursor.sort() : Returns results ordered according to a sort specification.

9. What is map reduce in MongoDB?

    Map-reduce is a data processing paradigm for condensing large volumes of data into useful aggregated results.
    In map-reduce operation, MongoDB applies the map phase to each input document.The map function emits key-value pairs.
    For those keys that have multiple values, MongoDB applies the reduce phase, which collects and condenses the aggregated data.

    Eg:
    var m1 = function() {
 
     {emit(this.Transaction_Detail.Item_Name, this.Transaction_Detail.Quantity)};
     
    }

    var r1 = function(name,quantity) {
     {return Array.sum(quantity)}
    }

    db.Transaction.mapReduce(m1,r1,{out:"totalll"})

------------------------------------------------------------------------------------------------    
                          Distributed Database Concepts

1. What is Distributed Database? Write Chracteristic of Distributed database.

    A Distributed Database Management System (DDBMS) contains a single logical 
    database that is divided into a number of fragments. 
    Every fragment gets stored on one or more computers under the control of a separate DBMS, 
    with the computers connected by a communications network.

    Chracteristic of Distributed database:

    1. logically interrealted
    2. Data fregmenents
    3. Fragment may get replicated
    4. replicas & Fragments are allocated to site
    5. connected network.
    6. DBMS
    7. Site Automous

2. Explain the need of Distributed DBMS.    

    A distributed database is basically a database that is not limited to one system, 
    it is spread over different sites, i.e, on multiple computers or over a network of computers. 

    Distributed databases offer some key advantages over centralized databases. 

    a. Reliability – Building an infrastructure is similar to investing: diversify to reduce your chances of loss. 
    b. Security – We can give permissions to single sections of the overall database, for better internal and external protection.
    c. Cost-effective – Bandwidth prices go down because users are accessing remote data less frequently.
    d. Local access - if there is a failure in the umbrella network, you can still get access to your portion of the database.
    e. Growth – If you add a new location to your business, it’s simple to create an additional node within the database, making distribution highly scalable.
    f. Speed & resource efficiency – Most requests and other interactivity with the database are performed at a local level, also decreasing remote traffic.

3. Write advantages & disadvantages of DDBS.

     Advantages 
    -------------  
 
    1. Reflections of organiasation  structure
    2. Imporved  sharing,local autonomoy
    3. Improved Availability of data
    4. Reliability of data
    5. Performance of backed is improved
    6. Economics support
    7. Modular growth 

     Disadvantages
    --------------

    1. Complexity(database design)
    2. security
    3. Lack of standards  

4. What are the types of DDBS?

    There are two types of DDBS:
        1. Homogeneous
        2. Heterogeneous

5. Explain the architectures of DDBS. 

    Three architectures are used in distributed database systems:

    a. Client/server Architecture : Client/server architectures are those in which a DBMS-related workload is split into two logical 
    components namely client and server, each of which typically executes on different systems. 
    Client is the user of the resource whereas the server is a provider of the resource.

    b. Peer- to-Peer Architecture for DDBMS: In these systems, each peer acts both as a client and a server for imparting database services. 
    The peers share their resource with other peers and co-ordinate their activities.

    c. Multi - DBMS Architectures : This is an integrated database system formed by a collection of two or more autonomous database systems.

6. Which are the design stategies of DDBS?  

    DESIGN OF DDBMS(distributed databases)
        Straegies:
        1. Fragmentation
        2.Allocation
        3.Replication

7. What is data allocation? Explain various allocation strategies.

    Allocation : After fragmentation each partition or the fragmment is store at a site
                (how much )

                Straegies             Local of reference    Reliability & Availability    Performance      Storage cost    Communication Cost

                centralized             Lowest level          Lowest                       Unsatisfactory     Lowest           Highest

                Fragmented               High                 High                          Satisfactory       low               Low

                Complete Replication     Highest              Highest                       Best                Highest          Lowest

                Selective Replication    High                 High                           Satisfactory         Low            Low


8. What is data replication? Explain various replication schemes. 

 Replication :Data replication is the process in which the data is copied at multiple locations (Different computers or servers) 
 to improve the availability of data.

    a. Partial replication : Partial replication means only some fragments are replicated from the database.
    b. Full replication : In full replication scheme, the database is available to almost every location or user in communication network.

9. What is Fragmentation? What are the types of fragmentation?

    Fragmentation :   Divide the whole db into subrelations
                    a. Horizontal 
                    b.Vertical 
                    c. Hybrid(based on predicate and projection)

10. Explain Horizontal Fragmentation with suitable example.

    Horizontal Fragmentation is splitting of tables horizontally that is into tuples or rows. 
    For example, a COMPANY table having 1000 records can be horizontally fragmented into ten fragments, 
    each fragment having 100 unique records.

     we can use SELECT statement, with the WHERE clause on a single attribute.


     emp_id | emp_name | job_name  | manager_id | hire_date  | salary  | commission | dep_id
--------+----------+-----------+------------+------------+---------+------------+--------
  68319 | KAYLING  | PRESIDENT |            | 1991-11-18 | 6000.00 |            |   1001
  66928 | BLAZE    | MANAGER   |      68319 | 1991-05-01 | 2750.00 |            |   3001
  67832 | CLARE    | MANAGER   |      68319 | 1991-06-09 | 2550.00 |            |   1001
  65646 | JONAS    | MANAGER   |      68319 | 1991-04-02 | 2957.00 |            |   2001
  67858 | SCARLET  | ANALYST   |      65646 | 1997-04-19 | 3100.00 |            |   2001
  69062 | FRANK    | ANALYST   |      65646 | 1991-12-03 | 3100.00 |            |   2001
  63679 | SANDRINE | CLERK     |      69062 | 1990-12-18 |  900.00 |            |   2001
  64989 | ADELYN   | SALESMAN  |      66928 | 1991-02-20 | 1700.00 |     400.00 |   3001


  here horizontal fragmentation is : 
  a.  66928 | BLAZE    | MANAGER   |      68319 | 1991-05-01 | 2750.00 |            |   3001

  b. 64989 | ADELYN   | SALESMAN  |      66928 | 1991-02-20 | 1700.00 |     400.00 |   3001

  every row is example of horizontal fragmentation.



11. Explain Vertical Fragmentation with suitable example.

    Vertical Fragmentation is fragmenting of table into columns known as set or site,
     where every site must have at least one column in common such as the primary key attribute column 
     (so that when the fragmented sites when needed can again be formed to a whole.

     For eg : 

      emp_id | emp_name | job_name  | manager_id | hire_date  | salary  | commission | dep_id
--------+----------+-----------+------------+------------+---------+------------+--------
  68319 | KAYLING  | PRESIDENT |            | 1991-11-18 | 6000.00 |            |   1001
  66928 | BLAZE    | MANAGER   |      68319 | 1991-05-01 | 2750.00 |            |   3001
  67832 | CLARE    | MANAGER   |      68319 | 1991-06-09 | 2550.00 |            |   1001
  65646 | JONAS    | MANAGER   |      68319 | 1991-04-02 | 2957.00 |            |   2001
  67858 | SCARLET  | ANALYST   |      65646 | 1997-04-19 | 3100.00 |            |   2001
  69062 | FRANK    | ANALYST   |      65646 | 1991-12-03 | 3100.00 |            |   2001
  63679 | SANDRINE | CLERK     |      69062 | 1990-12-18 |  900.00 |            |   2001
  64989 | ADELYN   | SALESMAN  |      66928 | 1991-02-20 | 1700.00 |     400.00 |   3001

here vertical fragmentation is : 

 emp_name               | job_name 
--------                +----------+
  66928                 | BLAZE    |
  67832                 | CLARE    |
  65646                 | JONAS    | 
  67858                 | SCARLET  | 
  69062                 | FRANK    | 
  63679                 | SANDRINE | 
  64989                 | ADELYN  


12. Explain Hybrid Fragmentation with suitable example.

    In hybrid fragmentation, a combination of horizontal and vertical fragmentation techniques are used. 
    This is the most flexible fragmentation technique since it generates fragments with minimal extraneous information. 

    Eg: 

    emp_id | emp_name | job_name  | manager_id | hire_date  | salary  | commission | dep_id
    ------+----------+-----------+------------+------------+---------+------------+--------
    68319 | KAYLING  | PRESIDENT |            | 1991-11-18 | 6000.00 |            |   1001
    66928 | BLAZE    | MANAGER   |      68319 | 1991-05-01 | 2750.00 |            |   3001
    67832 | CLARE    | MANAGER   |      68319 | 1991-06-09 | 2550.00 |            |   1001
    65646 | JONAS    | MANAGER   |      68319 | 1991-04-02 | 2957.00 |            |   2001
    67858 | SCARLET  | ANALYST   |      65646 | 1997-04-19 | 3100.00 |            |   2001
    69062 | FRANK    | ANALYST   |      65646 | 1991-12-03 | 3100.00 |            |   2001
    63679 | SANDRINE | CLERK     |      69062 | 1990-12-18 |  900.00 |            |   2001
    64989 | ADELYN   | SALESMAN  |      66928 | 1991-02-20 | 1700.00 |     400.00 |   3001





