FILM COLLECTION
---------------
db.Film.insert({"Film Id":100,"Title of Film":"Little Fires","Year of Release":2010,"genre":["drama","adventure"],"actors":[{"fname":"Lina","lname":"Duke"},{"fname":"Emma","lname":"watson"}],"directors":[{"fname":"Jhone","lname":"watson"},{"fname":"Steve","lname":"Smith"}],"release details":[{"places of release":["Newyok","Canada"]},{"dates":["1/1/2010","2/2/2010"]},{"rating of film":4}]})

db.Film.insert({"Film Id":101,"Title of Film":"Master","Year of Release":2020,"genre":["drama","action"],"actors":[{"fname":"Vijay","lname":"Thala"},{"fname":"Shruti","lname":"Hasan"}],"directors":[{"fname":"Prabhu","lname":"deva"},{"fname":"Remo","lname":"Desoza"}],"release details":[{"places of release":["Kerala","Karnataka"]},{"dates":["1/1/2020","2/2/2020"]},{"rating of film":3}]})

db.Film.insert({"Film Id":102,"Title of Film":"Dil Tho Pagal hai","Year of Release":1998,"genre":["drama","romantic"],"actors":[{"fname":"Shahrukh","lname":"Khan"},{"fname":"Madhuri","lname":"Dixit"}],"directors":[{"fname":"Yash","lname":"Chopra"},{"fname":"Karan","lname":"Johar"}],"release details":[{"places of release":["Mumbai","Goa"]},{"dates":["11/11/1998","12/12/1998"]},{"rating of film":5}]})

db.Film.insert({"Film Id":103,"Title of Film":"Rowdy Rathore","Year of Release":2009,"genre":["Triller","Action"],"actors":[{"fname":"Akshay","lname":"Kumar"},{"fname":"Sonakshi","lname":"Sinha"}],"directors":[{"fname":"Rohit","lname":"Shetty"},{"fname":"Ajay","lname":"Devgan"}],"release details":[{"places of release":["Mumbai","Banglore"]},{"dates":["11/11/2009","12/12/2011"]},{"rating of film":4}]})

db.Film.insert({"Film Id":104,"Title of Film":"Tiger Zinda Hai","Year of Release":2015,"genre":["Triller","Advanture"],"actors":[{"fname":"Salman","lname":"Khan"},{"fname":"Katrina","lname":"Kaif"}],"directors":[{"fname":"Ali Abbas ","lname":"Zafar"}],"release details":[{"places of release":["Agra","Banglore"]},{"dates":["11/11/2015","12/12/2015"]},{"rating of film":"A"}]})

db.Film.insert({"Film Id":105,"Title of Film":"Golmaal","Year of Release":2014,"genre":["Comedy","drama"],"actors":[{"fname":"Ajay","lname":"Devgan"},{"fname":"Kareena","lname":"Khan"}],"directors":[{"fname":"Rohit","lname":"Shetty"}],"release details":[{"places of release":["Delhi","Hyderabad"]},{"dates":["11/11/2014","12/12/2014"]},{"rating of film":"A"}]})

db.Film.insert({"Film Id":106,"Title of Film":"Tanhaji","Year of Release":2019,"genre":["action","adventure"],"actors":[{"fname":"Ajay","lname":"Devgan"},{"fname":"Kajol","lname":"Devgan"}],"directors":[{"fname":"Om","lname":"Raut"}],"release details":[{"places of release":["Mumbai","Pune"]},{"dates":["11/11/2019","12/12/2019"]},{"rating of film":"A"}]})

db.Film.insert({"Film Id":107,"Title of Film":"Titanic","Year of Release":1997,"genre":["Triller","romantic"],"actors":[{"fname":"Kate","lname":"Winslet"},{"fname":"Leonardo","lname":"Dicaprio"}],"directors":[{"fname":"James","lname":"Cameron"}],"release details":[{"places of release":["London"]},{"dates":["11/10/1997","12/11/1997"]},{"rating of film":"A"}]})

db.Film.insert({"Film Id":108,"Title of Film":"Endgame","Year of Release":2019,"genre":["Triller","action"],"actors":[{"fname":"Chris","lname":"Evans"},{"fname":"Robert","lname":"Downey"}],"directors":[{"fname":"Joe","lname":"Russo"}],"release details":[{"places of release":["India"]},{"dates":["11/10/2019","12/11/2019"]},{"rating of film":"A"}]})

db.Film.insert({"Film Id":109,"Title of Film":"Far from Home","Year of Release":2019,"genre":["Triller","action"],"actors":[{"fname":"Tom","lname":"Holland"},{"fname":"Zandya","lname":"Jones"}],"directors":[{"fname":"Jon","lname":"Watts"}],"release details":[{"places of release":["Japan"]},{"dates":["28/06/2019","29/06/2019"]},{"rating of film":"A"}]})

db.Film.insert({"Film Id":110,"Title of Film":"Koyla","Year of Release":1999,"genre":["drama","romantic"],"actors":[{"fname":"Shahrukh","lname":"Khan"},{"fname":"Madhuri","lname":"Dixit"}],"directors":[{"fname":"Yash","lname":"Chopra"},{"fname":"Karan","lname":"Johar"}],"release details":[{"places of release":["Mumbai","Pune"]},{"dates":["1/1/1999","2/2/1999"]},{"rating of film":"A"}]})
------------------------------------------

ACTOR COLLECTION
-----------------

db.Actor.insert({"Actor Id":11,"First Name":"Salman","Last Name":"Khan","Address":{"Street":"Bandra","City":"Mumbai","State":"Maharashtra","Country":"India","Pin-code":411010},"Contact Details":{"EmailId":"salu@gmail.com","PhoneNo":"8989785644"},"Age":52})

db.Actor.insert({"Actor Id":12,"First Name":"Shahrukh","Last Name":"Khan","Address":{"Street":"Bandra","City":"Mumbai","State":"Maharashtra","Country":"India","Pin-code":411010},"Contact Details":{"EmailId":"shahrukh@gmail.com","PhoneNo":"7789785644"},"Age":54})

db.Actor.insert({"Actor Id":13,"First Name":"Madhuri","Last Name":"Dixit","Address":{"Street":"Bangu","City":"Banglore","State":"Karnataka","Country":"India","Pin-code":411022},"Contact Details":{"EmailId":"madhu@gmail.com","PhoneNo":"7789005644"},"Age":50})

db.Actor.insert({"Actor Id":14,"First Name":"Emma","Last Name":"watson","Address":{"Street":"spine","City":"Madrid","State":"Valencia","Country":"Spain","Pin-code":991022},"Contact Details":{"EmailId":"emma@gmail.com","PhoneNo":"7789000044"},"Age":30})

db.Actor.insert({"Actor Id":15,"First Name":"Tom","Last Name":"Holland","Address":{"Street":"Kingston","City":"Thema","State":"London","Country":"England","Pin-code":811010},"Contact Details":{"EmailId":"tom@gmail.com","PhoneNo":"8988885644"},"Age":32})

db.Actor.insert({"Actor Id":16,"First Name":"Chris","Last Name":"Evans","Address":{"Street":"Wilshire Blvd","City":"Beverly","State":"Newyok","Country":"USA","Pin-code":511010},"Contact Details":{"EmailId":"chris@gmail.com","PhoneNo":"8988880000"},"Age":52})

------------------------------------

=======================================================================

1. Insert at least 10 documents in the collection Film ---

	a. Insert at least one document with film belonging to two genres. 
	b.Insert at least one document with film that is released at more than one place and on two different dates. 
	c.Insert at least three documents with the films released in the same year. 
	d. Insert at least two documents with the films directed by onedirector. 
	e. Insert at least two documents with films those are acted by a pair ‘Madhuri Dixit’ and ‘Shahrukh Khan’. 


	db.Film.insert({"Film Id":100,"Title of Film":"Little Fires","Year of Release":2010,"genre":["drama","adventure"],"actors":[{"fname":"Lina","lname":"Duke"},{"fname":"Emma","lname":"watson"}],"directors":[{"fname":"Jhone","lname":"watson"},{"fname":"Steve","lname":"Smith"}],"release details":[{"places of release":["Newyok","Canada"]},{"dates":["1/1/2010","2/2/2010"]},{"rating of film":4}]})

	db.Film.insert({"Film Id":101,"Title of Film":"Master","Year of Release":2020,"genre":["drama","action"],"actors":[{"fname":"Vijay","lname":"Thala"},{"fname":"Shruti","lname":"Hasan"}],"directors":[{"fname":"Prabhu","lname":"deva"},{"fname":"Remo","lname":"Desoza"}],"release details":[{"places of release":["Kerala","Karnataka"]},{"dates":["1/1/2020","2/2/2020"]},{"rating of film":3}]})

	db.Film.insert({"Film Id":102,"Title of Film":"Dil Tho Pagal hai","Year of Release":1998,"genre":["drama","romantic"],"actors":[{"fname":"Shahrukh","lname":"Khan"},{"fname":"Madhuri","lname":"Dixit"}],"directors":[{"fname":"Yash","lname":"Chopra"},{"fname":"Karan","lname":"Johar"}],"release details":[{"places of release":["Mumbai","Goa"]},{"dates":["11/11/1998","12/12/1998"]},{"rating of film":5}]})

	db.Film.insert({"Film Id":103,"Title of Film":"Rowdy Rathore","Year of Release":2009,"genre":["Triller","Action"],"actors":[{"fname":"Akshay","lname":"Kumar"},{"fname":"Sonakshi","lname":"Sinha"}],"directors":[{"fname":"Rohit","lname":"Shetty"},{"fname":"Ajay","lname":"Devgan"}],"release details":[{"places of release":["Mumbai","Banglore"]},{"dates":["11/11/2009","12/12/2011"]},{"rating of film":4}]})

	db.Film.insert({"Film Id":104,"Title of Film":"Tiger Zinda Hai","Year of Release":2015,"genre":["Triller","Advanture"],"actors":[{"fname":"Salman","lname":"Khan"},{"fname":"Katrina","lname":"Kaif"}],"directors":[{"fname":"Ali Abbas ","lname":"Zafar"}],"release details":[{"places of release":["Agra","Banglore"]},{"dates":["11/11/2015","12/12/2015"]},{"rating of film":"A"}]})

	db.Film.insert({"Film Id":105,"Title of Film":"Golmaal","Year of Release":2013,"genre":["Comedy","drama"],"actors":[{"fname":"Ajay","lname":"Devgan"},{"fname":"Kareena","lname":"Khan"}],"directors":[{"fname":"Rohit","lname":"Shetty"}],"release details":[{"places of release":["Delhi","Hyderabad"]},{"dates":["11/11/2013","12/12/2013"]},{"rating of film":"A"}]})

	db.Film.insert({"Film Id":106,"Title of Film":"Tanhaji","Year of Release":2019,"genre":["action","adventure"],"actors":[{"fname":"Ajay","lname":"Devgan"},{"fname":"Kajol","lname":"Devgan"}],"directors":[{"fname":"Om","lname":"Raut"}],"release details":[{"places of release":["Mumbai","Pune"]},{"dates":["11/11/2019","12/12/2019"]},{"rating of film":"A"}]})

	db.Film.insert({"Film Id":107,"Title of Film":"Titanic","Year of Release":1997,"genre":["Triller","romantic"],"actors":[{"fname":"Kate","lname":"Winslet"},{"fname":"Leonardo","lname":"Dicaprio"}],"directors":[{"fname":"James","lname":"Cameron"}],"release details":[{"places of release":["London"]},{"dates":["11/10/1997","12/11/1997"]},{"rating of film":"A"}]})

	db.Film.insert({"Film Id":108,"Title of Film":"Endgame","Year of Release":2019,"genre":["Triller","action"],"actors":[{"fname":"Chris","lname":"Evans"},{"fname":"Robert","lname":"Downey"}],"directors":[{"fname":"Joe","lname":"Russo"}],"release details":[{"places of release":["India"]},{"dates":["11/10/2019","12/11/2019"]},{"rating of film":"A"}]})

	db.Film.insert({"Film Id":109,"Title of Film":"Far from Home","Year of Release":2019,"genre":["Triller","action"],"actors":[{"fname":"Tom","lname":"Holland"},{"fname":"Zandya","lname":"Jones"}],"directors":[{"fname":"Jon","lname":"Watts"}],"release details":[{"places of release":["Japan"]},{"dates":["28/06/2019","29/06/2019"]},{"rating of film":"A"}]})

	db.Film.insert({"Film Id":110,"Title of Film":"Koyla","Year of Release":1999,"genre":["drama","romantic"],"actors":[{"fname":"Shahrukh","lname":"Khan"},{"fname":"Madhuri","lname":"Dixit"}],"directors":[{"fname":"Yash","lname":"Chopra"},{"fname":"Karan","lname":"Johar"}],"release details":[{"places of release":["Mumbai","Pune"]},{"dates":["1/1/1999","2/2/1999"]},{"rating of film":"A"}]})
	------------------------------------------

2. Insert at least 10 documents in the collection Actor. 

	db.Actor.insert({"Actor Id":11,"First Name":"Salman","Last Name":"Khan","Address":{"Street":"Bandra","City":"Mumbai","State":"Maharashtra","Country":"India","Pin-code":411010},"Contact Details":{"EmailId":"salu@gmail.com","PhoneNo":"8989785644"},"Age":52})

	db.Actor.insert({"Actor Id":12,"First Name":"Shahrukh","Last Name":"Khan","Address":{"Street":"Bandra","City":"Mumbai","State":"Maharashtra","Country":"India","Pin-code":411010},"Contact Details":{"EmailId":"shahrukh@gmail.com","PhoneNo":"7789785644"},"Age":54})

	db.Actor.insert({"Actor Id":13,"First Name":"Madhuri","Last Name":"Dixit","Address":{"Street":"Bangu","City":"Banglore","State":"Karnataka","Country":"India","Pin-code":411022},"Contact Details":{"EmailId":"madhu@gmail.com","PhoneNo":"7789005644"},"Age":50})

	db.Actor.insert({"Actor Id":14,"First Name":"Emma","Last Name":"watson","Address":{"Street":"spine","City":"Madrid","State":"Valencia","Country":"Spain","Pin-code":991022},"Contact Details":{"EmailId":"emma@gmail.com","PhoneNo":"7789000044"},"Age":30})

	db.Actor.insert({"Actor Id":15,"First Name":"Tom","Last Name":"Holland","Address":{"Street":"Kingston","City":"Thema","State":"London","Country":"England","Pin-code":811010},"Contact Details":{"EmailId":"tom@gmail.com","PhoneNo":"8988885644"},"Age":32})

	db.Actor.insert({"Actor Id":16,"First Name":"Chris","Last Name":"Evans","Address":{"Street":"Wilshire Blvd","City":"Beverly","State":"Newyok","Country":"USA","Pin-code":511010},"Contact Details":{"EmailId":"chris@gmail.com","PhoneNo":"8988880000"},"Age":52})

-------------------------------------

3. Display all the documents inserted in both the collections. 

> db.Film.find().pretty()
{
	"_id" : ObjectId("6030ca3b5ac0891cbf1d891e"),
	"Film Id" : 100,
	"Title of Film" : "Little Fires",
	"Year of Release" : 2010,
	"genre" : [
		"drama",
		"adventure"
	],
	"actors" : [
		{
			"fname" : "Lina",
			"lname" : "Duke"
		},
		{
			"fname" : "Emma",
			"lname" : "watson"
		}
	],
	"directors" : [
		{
			"fname" : "Jhone",
			"lname" : "watson"
		},
		{
			"fname" : "Steve",
			"lname" : "Smith"
		}
	],
	"release details" : [
		{
			"places of release" : [
				"Newyok",
				"Canada"
			]
		},
		{
			"dates" : [
				"1/1/2010",
				"2/2/2010"
			]
		},
		{
			"rating of film" : 4
		}
	]
}
{
	"_id" : ObjectId("6030caf95ac0891cbf1d891f"),
	"Film Id" : 101,
	"Title of Film" : "Master",
	"Year of Release" : 2020,
	"genre" : [
		"drama",
		"action"
	],
	"actors" : [
		{
			"fname" : "Vijay",
			"lname" : "Thala"
		},
		{
			"fname" : "Shruti",
			"lname" : "Hasan"
		}
	],
	"directors" : [
		{
			"fname" : "Prabhu",
			"lname" : "deva"
		},
		{
			"fname" : "Remo",
			"lname" : "Desoza"
		}
	],
	"release details" : [
		{
			"places of release" : [
				"Kerala",
				"Karnataka"
			]
		},
		{
			"dates" : [
				"1/1/2020",
				"2/2/2020"
			]
		},
		{
			"rating of film" : 3
		}
	]
}
{
	"_id" : ObjectId("6030cbf15ac0891cbf1d8920"),
	"Film Id" : 102,
	"Title of Film" : "Dil Tho Pagal hai",
	"Year of Release" : 1998,
	"genre" : [
		"drama",
		"romantic"
	],
	"actors" : [
		{
			"fname" : "Shahrukh",
			"lname" : "Khan"
		},
		{
			"fname" : "Madhuri",
			"lname" : "Dixit"
		}
	],
	"directors" : [
		{
			"fname" : "Yash",
			"lname" : "Chopra"
		},
		{
			"fname" : "Karan",
			"lname" : "Johar"
		}
	],
	"release details" : [
		{
			"places of release" : [
				"Mumbai",
				"Goa"
			]
		},
		{
			"dates" : [
				"11/11/1998",
				"12/12/1998"
			]
		},
		{
			"rating of film" : 5
		}
	]
}
{
	"_id" : ObjectId("6030cd9d5ac0891cbf1d8921"),
	"Film Id" : 103,
	"Title of Film" : "Rowdy Rathore",
	"Year of Release" : 2009,
	"genre" : [
		"Triller",
		"Action"
	],
	"actors" : [
		{
			"fname" : "Akshay",
			"lname" : "Kumar"
		},
		{
			"fname" : "Sonakshi",
			"lname" : "Sinha"
		}
	],
	"directors" : [
		{
			"fname" : "Rohit",
			"lname" : "Shetty"
		},
		{
			"fname" : "Ajay",
			"lname" : "Devgan"
		}
	],
	"release details" : [
		{
			"places of release" : [
				"Mumbai",
				"Banglore"
			]
		},
		{
			"dates" : [
				"11/11/2009",
				"12/12/2011"
			]
		},
		{
			"rating of film" : 4
		}
	]
}
{
	"_id" : ObjectId("6030cf235ac0891cbf1d8922"),
	"Film Id" : 104,
	"Title of Film" : "Tiger Zinda Hai",
	"Year of Release" : 2015,
	"genre" : [
		"Triller",
		"Advanture"
	],
	"actors" : [
		{
			"fname" : "Salman",
			"lname" : "Khan"
		},
		{
			"fname" : "Katrina",
			"lname" : "Kaif"
		}
	],
	"directors" : [
		{
			"fname" : "Ali Abbas ",
			"lname" : "Zafar"
		}
	],
	"release details" : [
		{
			"places of release" : [
				"Agra",
				"Banglore"
			]
		},
		{
			"dates" : [
				"11/11/2015",
				"12/12/2015"
			]
		},
		{
			"rating of film" : "A"
		}
	]
}
{
	"_id" : ObjectId("6030d0b95ac0891cbf1d8924"),
	"Film Id" : 105,
	"Title of Film" : "Golmaal",
	"Year of Release" : 2013,
	"genre" : [
		"Comedy",
		"drama"
	],
	"actors" : [
		{
			"fname" : "Ajay",
			"lname" : "Devgan"
		},
		{
			"fname" : "Kareena",
			"lname" : "Khan"
		}
	],
	"directors" : [
		{
			"fname" : "Rohit",
			"lname" : "Shetty"
		}
	],
	"release details" : [
		{
			"places of release" : [
				"Delhi",
				"Hyderabad"
			]
		},
		{
			"dates" : [
				"11/11/2013",
				"12/12/2013"
			]
		},
		{
			"rating of film" : "A"
		}
	]
}
{
	"_id" : ObjectId("6030d15f5ac0891cbf1d8925"),
	"Film Id" : 106,
	"Title of Film" : "Tanhaji",
	"Year of Release" : 2019,
	"genre" : [
		"action",
		"adventure"
	],
	"actors" : [
		{
			"fname" : "Ajay",
			"lname" : "Devgan"
		},
		{
			"fname" : "Kajol",
			"lname" : "Devgan"
		}
	],
	"directors" : [
		{
			"fname" : "Om",
			"lname" : "Raut"
		}
	],
	"release details" : [
		{
			"places of release" : [
				"Mumbai",
				"Pune"
			]
		},
		{
			"dates" : [
				"11/11/2019",
				"12/12/2019"
			]
		},
		{
			"rating of film" : "A"
		}
	]
}
{
	"_id" : ObjectId("6030d64f5ac0891cbf1d892a"),
	"Film Id" : 107,
	"Title of Film" : "Titanic",
	"Year of Release" : 1997,
	"genre" : [
		"Triller",
		"romantic"
	],
	"actors" : [
		{
			"fname" : "Kate",
			"lname" : "Winslet"
		},
		{
			"fname" : "Leonardo",
			"lname" : "Dicaprio"
		}
	],
	"directors" : [
		{
			"fname" : "James",
			"lname" : "Cameron"
		}
	],
	"release details" : [
		{
			"places of release" : [
				"London"
			]
		},
		{
			"dates" : [
				"11/10/1997",
				"12/11/1997"
			]
		},
		{
			"rating of film" : "A"
		}
	]
}
{
	"_id" : ObjectId("6030d70e5ac0891cbf1d892b"),
	"Film Id" : 108,
	"Title of Film" : "Endgame",
	"Year of Release" : 2019,
	"genre" : [
		"Triller",
		"action"
	],
	"actors" : [
		{
			"fname" : "Chris",
			"lname" : "Evans"
		},
		{
			"fname" : "Robert",
			"lname" : "Downey"
		}
	],
	"directors" : [
		{
			"fname" : "Joe",
			"lname" : "Russo"
		}
	],
	"release details" : [
		{
			"places of release" : [
				"India"
			]
		},
		{
			"dates" : [
				"11/10/2019",
				"12/11/2019"
			]
		},
		{
			"rating of film" : "A"
		}
	]
}
{
	"_id" : ObjectId("6030d7a85ac0891cbf1d892c"),
	"Film Id" : 109,
	"Title of Film" : "Far from Home",
	"Year of Release" : 2019,
	"genre" : [
		"Triller",
		"action"
	],
	"actors" : [
		{
			"fname" : "Tom",
			"lname" : "Holland"
		},
		{
			"fname" : "Zandya",
			"lname" : "Jones"
		}
	],
	"directors" : [
		{
			"fname" : "Jon",
			"lname" : "Watts"
		}
	],
	"release details" : [
		{
			"places of release" : [
				"Japan"
			]
		},
		{
			"dates" : [
				"28/06/2019",
				"29/06/2019"
			]
		},
		{
			"rating of film" : "A"
		}
	]
}
{
	"_id" : ObjectId("6030d85d5ac0891cbf1d892d"),
	"Film Id" : 110,
	"Title of Film" : "Koyla",
	"Year of Release" : 1999,
	"genre" : [
		"drama",
		"romantic"
	],
	"actors" : [
		{
			"fname" : "Shahrukh",
			"lname" : "Khan"
		},
		{
			"fname" : "Madhuri",
			"lname" : "Dixit"
		}
	],
	"directors" : [
		{
			"fname" : "Yash",
			"lname" : "Chopra"
		},
		{
			"fname" : "Karan",
			"lname" : "Johar"
		}
	],
	"release details" : [
		{
			"places of release" : [
				"Mumbai",
				"Pune"
			]
		},
		{
			"dates" : [
				"1/1/1999",
				"2/2/1999"
			]
		},
		{
			"rating of film" : "A"
		}
	]
}


> db.Actor.find().pretty()
{
	"_id" : ObjectId("6030d2e85ac0891cbf1d8926"),
	"Actor Id" : 11,
	"First Name" : "Salman",
	"Last Name" : "Khan",
	"Address" : {
		"Street" : "Bandra",
		"City" : "Mumbai",
		"State" : "Maharashtra",
		"Country" : "India",
		"Pin-code" : 411010
	},
	"Contact Details" : {
		"EmailId" : "salu@gmail.com",
		"PhoneNo" : "8989785644"
	},
	"Age" : 52
}
{
	"_id" : ObjectId("6030d33b5ac0891cbf1d8927"),
	"Actor Id" : 12,
	"First Name" : "Shahrukh",
	"Last Name" : "Khan",
	"Address" : {
		"Street" : "Bandra",
		"City" : "Mumbai",
		"State" : "Maharashtra",
		"Country" : "India",
		"Pin-code" : 411010
	},
	"Contact Details" : {
		"EmailId" : "shahrukh@gmail.com",
		"PhoneNo" : "7789785644"
	},
	"Age" : 54
}
{
	"_id" : ObjectId("6030d3de5ac0891cbf1d8928"),
	"Actor Id" : 13,
	"First Name" : "Madhuri",
	"Last Name" : "Dixit",
	"Address" : {
		"Street" : "Bangu",
		"City" : "Banglore",
		"State" : "Karnataka",
		"Country" : "India",
		"Pin-code" : 411022
	},
	"Contact Details" : {
		"EmailId" : "madhu@gmail.com",
		"PhoneNo" : "7789005644"
	},
	"Age" : 50
}
{
	"_id" : ObjectId("6030d4c95ac0891cbf1d8929"),
	"Actor Id" : 14,
	"First Name" : "Emma",
	"Last Name" : "watson",
	"Address" : {
		"Street" : "spine",
		"City" : "Madrid",
		"State" : "Valencia",
		"Country" : "Spain",
		"Pin-code" : 991022
	},
	"Contact Details" : {
		"EmailId" : "emma@gmail.com",
		"PhoneNo" : "7789000044"
	},
	"Age" : 30
}
{
	"_id" : ObjectId("6030da105ac0891cbf1d892e"),
	"Actor Id" : 15,
	"First Name" : "Tom",
	"Last Name" : "Holland",
	"Address" : {
		"Street" : "Kingston",
		"City" : "Thema",
		"State" : "London",
		"Country" : "England",
		"Pin-code" : 811010
	},
	"Contact Details" : {
		"EmailId" : "tom@gmail.com",
		"PhoneNo" : "8988885644"
	},
	"Age" : 32
}
{
	"_id" : ObjectId("6030da995ac0891cbf1d892f"),
	"Actor Id" : 16,
	"First Name" : "Chris",
	"Last Name" : "Evans",
	"Address" : {
		"Street" : "Wilshire Blvd",
		"City" : "Beverly",
		"State" : "Newyok",
		"Country" : "USA",
		"Pin-code" : 511010
	},
	"Contact Details" : {
		"EmailId" : "chris@gmail.com",
		"PhoneNo" : "8988880000"
	},
	"Age" : 52
}
> ============================

4. Add a value to the rating of the film whose title starts with ‘T’.

db.Film.update({"Title of Film":/^T/},{$set:{"release details":[{"rating of film":"A+"}]}},{multi:true})
WriteResult({ "nMatched" : 3, "nUpserted" : 0, "nModified" : 2 })
> db.Film.find({"Title of Film":/^T/})
{ "_id" : ObjectId("6035d591febaf91510509bbe"), "Film Id" : 106, "Title of Film" : "Tanhaji", "Year of Release" : 2019, "genre" : [ "action", "adventure" ], "actors" : [ { "fname" : "Ajay", "lname" : "Devgan" }, { "fname" : "Kajol", "lname" : "Devgan" } ], "directors" : [ { "fname" : "Om", "lname" : "Raut" } ], "release details" : [ { "rating of film" : "A+" } ] }
{ "_id" : ObjectId("6035d591febaf91510509bbf"), "Film Id" : 107, "Title of Film" : "Titanic", "Year of Release" : 1997, "genre" : [ "Triller", "romantic" ], "actors" : [ { "fname" : "Kate", "lname" : "Winslet" }, { "fname" : "Leonardo", "lname" : "Dicaprio" } ], "directors" : [ { "fname" : "James", "lname" : "Cameron" } ], "release details" : [ { "rating of film" : "A+" } ] }
{ "_id" : ObjectId("60367c0a82eafd4bdbb8504b"), "Film Id" : 104, "Title of Film" : "Tiger Zinda Hai", "Year of Release" : 2015, "genre" : [ "Triller", "Advanture" ], "actors" : [ { "fname" : "Salman", "lname" : "Khan" }, { "fname" : "Katrina", "lname" : "Kaif" } ], "directors" : [ { "fname" : "Ali Abbas ", "lname" : "Zafar" } ], "release details" : [ { "rating of film" : "A+" } ] }
> 

--------------------
5. Add an actor named "Salman Khan " in the ‘Actor’ collection. 
Also add the details of the film in ‘Film’ collection in which this actor has acted in. 


db.Actor.insert({"Actor Id":11,"First Name":"Salman","Last Name":"Khan","Address":{"Street":"Bandra","City":"Mumbai","State":"Maharashtra","Country":"India","Pin-code":411010},"Contact Details":{"EmailId":"salu@gmail.com","PhoneNo":"8989785644"},"Age":52})

db.Film.insert({"Film Id":104,"Title of Film":"Tiger Zinda Hai","Year of Release":2015,"genre":["Triller","Advanture"],"actors":[{"fname":"Salman","lname":"Khan"},{"fname":"Katrina","lname":"Kaif"}],"directors":[{"fname":"Ali Abbas ","lname":"Zafar"}],"release details":[{"places of release":["Agra","Banglore"]},{"dates":["11/11/2015","12/12/2015"]},{"rating of film":"A"}]})

--------------------------
6. Delete the film "Master".

db.Film.remove({"Title of Film":"Master"})
WriteResult({ "nRemoved" : 1 })

-----------------------------
7. Delete an actor named "Tom Holland".

> db.Actor.remove({"First Name":"Tom"})
WriteResult({ "nRemoved" : 1 })
------------------------

8. Delete all actors from an ‘Actor’ collection who have age greater than “53” 

> db.Actor.remove({"Age":{$gt:53}})
WriteResult({ "nRemoved" : 1 })


--------------------------
9. Update the actor’s address where Actor Id is “13”. 


db.Actor.update({"Actor Id":13},{$set:{"Address":{"Street":"Bandra","City":"Mumbai","State":"Maharashtra","Country":"India","Pin-code": 411091}}})
WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })

> db.Actor.find({"Actor Id":13})
{ "_id" : ObjectId("6030d3de5ac0891cbf1d8928"), "Actor Id" : 13, "First Name" : "Madhuri", "Last Name" : "Dixit", "Address" : { "Street" : "Bandra", "City" : "Mumbai", "State" : "Maharashtra", "Country" : "India", "Pin-code" : 411091 }, "Contact Details" : { "EmailId" : "madhu@gmail.com", "PhoneNo" : "7789005644" }, "Age" : 50 }

----------------------------
10.Update the genre of the film directed by “Jhone”. 

> db.Film.update({"directors.fname":"Jhone"},{$set:{"genre":["action","adventure"]}})
WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })
> db.Film.find({"directors.fname":"Jhone"})
{ "_id" : ObjectId("6030ca3b5ac0891cbf1d891e"), "Film Id" : 100, "Title of Film" : "Little Fires", "Year of Release" : 2010, "genre" : [ "action", "adventure" ], "actors" : [ { "fname" : "Lina", "lname" : "Duke" }, { "fname" : "Emma", "lname" : "watson" } ], "directors" : [ { "fname" : "Jhone", "lname" : "watson" }, { "fname" : "Steve", "lname" : "Smith" } ], "release details" : [ { "places of release" : [ "Newyok", "Canada" ] }, { "dates" : [ "1/1/2010", "2/2/2010" ] }, { "rating of film" : 4 } ] }
> 

--------------------++++++++++++++++-------------------------------

(MongoDB Aggregate frameworkbased queries)

1. Find the titles of all the films starting with the letter ‘R’ released during the year 2009 and 2011.

 db.Film.find({"Year of Release":{$gt:2008,$lt:2012},"Title of Film":{$regex:"R"}},{_id:0,"Title of Film":1})
{ "Title of Film" : "Rowdy Rathore" }

------
2. Find the list of films acted by an actor "Madhuri".

> db.Film.find({"actors.fname":"Madhuri"},{_id:0,"Title of Film":1})
{ "Title of Film" : "Dil Tho Pagal hai" }
{ "Title of Film" : "Koyla" }

--------------------------

3. Find all the films released in 90s.

> db.Film.find({"Year of Release":{$gt:1900,$lt:2000}},{_id:0,"Title of Film":1}).pretty()
{ "Title of Film" : "Dil Tho Pagal hai" }
{ "Title of Film" : "Titanic" }
{ "Title of Film" : "Koyla" }
-----------------------------

4. Find all films belonging to “Adventure” and “Thriller” genre.

> db.Film.find({"genre":["Triller","Advanture"]},{_id:0,"Title of Film":1})
{ "Title of Film" : "Tiger Zinda Hai" }
-----------------------------------

5. Find all the films having ‘A’ rating.

> db.Film.find({"release details.rating of film":"A"},{_id:0,"Title of Film":1})
{ "Title of Film" : "Tiger Zinda Hai" }
{ "Title of Film" : "Golmaal" }
{ "Title of Film" : "Tanhaji" }
{ "Title of Film" : "Titanic" }
{ "Title of Film" : "Endgame" }
{ "Title of Film" : "Far from Home" }
{ "Title of Film" : "Koyla" }

------------------------

6. Arrange the film names in ascending order and release year should be in descending order. 


> db.Film.find().sort({"Title of Film":1,"Year of Release":-1}).pretty()
{
	"_id" : ObjectId("6030cbf15ac0891cbf1d8920"),
	"Film Id" : 102,
	"Title of Film" : "Dil Tho Pagal hai",
	"Year of Release" : 1998,
	"genre" : [
		"drama",
		"romantic"
	],
	"actors" : [
		{
			"fname" : "Shahrukh",
			"lname" : "Khan"
		},
		{
			"fname" : "Madhuri",
			"lname" : "Dixit"
		}
	],
	"directors" : [
		{
			"fname" : "Yash",
			"lname" : "Chopra"
		},
		{
			"fname" : "Karan",
			"lname" : "Johar"
		}
	],
	"release details" : [
		{
			"places of release" : [
				"Mumbai",
				"Goa"
			]
		},
		{
			"dates" : [
				"11/11/1998",
				"12/12/1998"
			]
		},
		{
			"rating of film" : 5
		}
	]
}
{
	"_id" : ObjectId("6030d70e5ac0891cbf1d892b"),
	"Film Id" : 108,
	"Title of Film" : "Endgame",
	"Year of Release" : 2019,
	"genre" : [
		"Triller",
		"action"
	],
	"actors" : [
		{
			"fname" : "Chris",
			"lname" : "Evans"
		},
		{
			"fname" : "Robert",
			"lname" : "Downey"
		}
	],
	"directors" : [
		{
			"fname" : "Joe",
			"lname" : "Russo"
		}
	],
	"release details" : [
		{
			"places of release" : [
				"India"
			]
		},
		{
			"dates" : [
				"11/10/2019",
				"12/11/2019"
			]
		},
		{
			"rating of film" : "A"
		}
	]
}
{
	"_id" : ObjectId("6030d7a85ac0891cbf1d892c"),
	"Film Id" : 109,
	"Title of Film" : "Far from Home",
	"Year of Release" : 2019,
	"genre" : [
		"Triller",
		"action"
	],
	"actors" : [
		{
			"fname" : "Tom",
			"lname" : "Holland"
		},
		{
			"fname" : "Zandya",
			"lname" : "Jones"
		}
	],
	"directors" : [
		{
			"fname" : "Jon",
			"lname" : "Watts"
		}
	],
	"release details" : [
		{
			"places of release" : [
				"Japan"
			]
		},
		{
			"dates" : [
				"28/06/2019",
				"29/06/2019"
			]
		},
		{
			"rating of film" : "A"
		}
	]
}
{
	"_id" : ObjectId("6030d0b95ac0891cbf1d8924"),
	"Film Id" : 105,
	"Title of Film" : "Golmaal",
	"Year of Release" : 2013,
	"genre" : [
		"Comedy",
		"drama"
	],
	"actors" : [
		{
			"fname" : "Ajay",
			"lname" : "Devgan"
		},
		{
			"fname" : "Kareena",
			"lname" : "Khan"
		}
	],
	"directors" : [
		{
			"fname" : "Rohit",
			"lname" : "Shetty"
		}
	],
	"release details" : [
		{
			"places of release" : [
				"Delhi",
				"Hyderabad"
			]
		},
		{
			"dates" : [
				"11/11/2013",
				"12/12/2013"
			]
		},
		{
			"rating of film" : "A"
		}
	]
}
{
	"_id" : ObjectId("6030d85d5ac0891cbf1d892d"),
	"Film Id" : 110,
	"Title of Film" : "Koyla",
	"Year of Release" : 1999,
	"genre" : [
		"drama",
		"romantic"
	],
	"actors" : [
		{
			"fname" : "Shahrukh",
			"lname" : "Khan"
		},
		{
			"fname" : "Madhuri",
			"lname" : "Dixit"
		}
	],
	"directors" : [
		{
			"fname" : "Yash",
			"lname" : "Chopra"
		},
		{
			"fname" : "Karan",
			"lname" : "Johar"
		}
	],
	"release details" : [
		{
			"places of release" : [
				"Mumbai",
				"Pune"
			]
		},
		{
			"dates" : [
				"1/1/1999",
				"2/2/1999"
			]
		},
		{
			"rating of film" : "A"
		}
	]
}
{
	"_id" : ObjectId("6030ca3b5ac0891cbf1d891e"),
	"Film Id" : 100,
	"Title of Film" : "Little Fires",
	"Year of Release" : 2010,
	"genre" : [
		"action",
		"adventure"
	],
	"actors" : [
		{
			"fname" : "Lina",
			"lname" : "Duke"
		},
		{
			"fname" : "Emma",
			"lname" : "watson"
		}
	],
	"directors" : [
		{
			"fname" : "Jhone",
			"lname" : "watson"
		},
		{
			"fname" : "Steve",
			"lname" : "Smith"
		}
	],
	"release details" : [
		{
			"places of release" : [
				"Newyok",
				"Canada"
			]
		},
		{
			"dates" : [
				"1/1/2010",
				"2/2/2010"
			]
		},
		{
			"rating of film" : 4
		}
	]
}
{
	"_id" : ObjectId("6030cd9d5ac0891cbf1d8921"),
	"Film Id" : 103,
	"Title of Film" : "Rowdy Rathore",
	"Year of Release" : 2009,
	"genre" : [
		"Triller",
		"Action"
	],
	"actors" : [
		{
			"fname" : "Akshay",
			"lname" : "Kumar"
		},
		{
			"fname" : "Sonakshi",
			"lname" : "Sinha"
		}
	],
	"directors" : [
		{
			"fname" : "Rohit",
			"lname" : "Shetty"
		},
		{
			"fname" : "Ajay",
			"lname" : "Devgan"
		}
	],
	"release details" : [
		{
			"places of release" : [
				"Mumbai",
				"Banglore"
			]
		},
		{
			"dates" : [
				"11/11/2009",
				"12/12/2011"
			]
		},
		{
			"rating of film" : 4
		}
	]
}
{
	"_id" : ObjectId("6030d15f5ac0891cbf1d8925"),
	"Film Id" : 106,
	"Title of Film" : "Tanhaji",
	"Year of Release" : 2019,
	"genre" : [
		"action",
		"adventure"
	],
	"actors" : [
		{
			"fname" : "Ajay",
			"lname" : "Devgan"
		},
		{
			"fname" : "Kajol",
			"lname" : "Devgan"
		}
	],
	"directors" : [
		{
			"fname" : "Om",
			"lname" : "Raut"
		}
	],
	"release details" : [
		{
			"places of release" : [
				"Mumbai",
				"Pune"
			]
		},
		{
			"dates" : [
				"11/11/2019",
				"12/12/2019"
			]
		},
		{
			"rating of film" : "A"
		}
	]
}
{
	"_id" : ObjectId("6030cf235ac0891cbf1d8922"),
	"Film Id" : 104,
	"Title of Film" : "Tiger Zinda Hai",
	"Year of Release" : 2015,
	"genre" : [
		"Triller",
		"Advanture"
	],
	"actors" : [
		{
			"fname" : "Salman",
			"lname" : "Khan"
		},
		{
			"fname" : "Katrina",
			"lname" : "Kaif"
		}
	],
	"directors" : [
		{
			"fname" : "Ali Abbas ",
			"lname" : "Zafar"
		}
	],
	"release details" : [
		{
			"places of release" : [
				"Agra",
				"Banglore"
			]
		},
		{
			"dates" : [
				"11/11/2015",
				"12/12/2015"
			]
		},
		{
			"rating of film" : "A"
		}
	]
}
{
	"_id" : ObjectId("6030d64f5ac0891cbf1d892a"),
	"Film Id" : 107,
	"Title of Film" : "Titanic",
	"Year of Release" : 1997,
	"genre" : [
		"Triller",
		"romantic"
	],
	"actors" : [
		{
			"fname" : "Kate",
			"lname" : "Winslet"
		},
		{
			"fname" : "Leonardo",
			"lname" : "Dicaprio"
		}
	],
	"directors" : [
		{
			"fname" : "James",
			"lname" : "Cameron"
		}
	],
	"release details" : [
		{
			"places of release" : [
				"London"
			]
		},
		{
			"dates" : [
				"11/10/1997",
				"12/11/1997"
			]
		},
		{
			"rating of film" : "A"
		}
	]
}
> 

-------------------------
7. Sort the actors in ascending order according to their age.

> db.Actor.find().sort({"Age":1}).pretty()
{
	"_id" : ObjectId("6030d4c95ac0891cbf1d8929"),
	"Actor Id" : 14,
	"First Name" : "Emma",
	"Last Name" : "watson",
	"Address" : {
		"Street" : "spine",
		"City" : "Madrid",
		"State" : "Valencia",
		"Country" : "Spain",
		"Pin-code" : 991022
	},
	"Contact Details" : {
		"EmailId" : "emma@gmail.com",
		"PhoneNo" : "7789000044"
	},
	"Age" : 30
}
{
	"_id" : ObjectId("6030d3de5ac0891cbf1d8928"),
	"Actor Id" : 13,
	"First Name" : "Madhuri",
	"Last Name" : "Dixit",
	"Address" : {
		"Street" : "Bandra",
		"City" : "Mumbai",
		"State" : "Maharashtra",
		"Country" : "India",
		"Pin-code" : 411091
	},
	"Contact Details" : {
		"EmailId" : "madhu@gmail.com",
		"PhoneNo" : "7789005644"
	},
	"Age" : 50
}
{
	"_id" : ObjectId("6030d2e85ac0891cbf1d8926"),
	"Actor Id" : 11,
	"First Name" : "Salman",
	"Last Name" : "Khan",
	"Address" : {
		"Street" : "Bandra",
		"City" : "Mumbai",
		"State" : "Maharashtra",
		"Country" : "India",
		"Pin-code" : 411010
	},
	"Contact Details" : {
		"EmailId" : "salu@gmail.com",
		"PhoneNo" : "8989785644"
	},
	"Age" : 52
}
{
	"_id" : ObjectId("6030da995ac0891cbf1d892f"),
	"Actor Id" : 16,
	"First Name" : "Chris",
	"Last Name" : "Evans",
	"Address" : {
		"Street" : "Wilshire Blvd",
		"City" : "Beverly",
		"State" : "Newyok",
		"Country" : "USA",
		"Pin-code" : 511010
	},
	"Contact Details" : {
		"EmailId" : "chris@gmail.com",
		"PhoneNo" : "8988880000"
	},
	"Age" : 52
}
> 

----------------------------------

8. Find movies that are comedies or dramas and are released after 2013.

> db.Film.find({$and:[{"genre":["Comedy","drama"]},{"Year of Release":{$gt:2013}}]},{_id:0,"Title of Film":1});
{ "Title of Film" : "Golmaal" }


--------------------------------

9. Show the latest 2 films acted by an actor “Madhuri”. 

> db.Film.find({"actors.fname":"Madhuri"},{_id:0,"Title of Film":1}).sort({_id:-1}).limit(2)
{ "Title of Film" : "Koyla" }
{ "Title of Film" : "Dil Tho Pagal hai" }

-----------------------------------
10. List the titles of films acted by actors “ Shahrukh” and “Madhuri ”.

 db.Film.find({$and:[{"actors.fname":"Shahrukh"},{"actors.fname":"Madhuri"}]},{_id:0,"Title of Film":1})
{ "Title of Film" : "Dil Tho Pagal hai" }
{ "Title of Film" : "Koyla" }

-------------------------

11.Retrieve films with an actor living in Spain. 


> var addressDetail = db.Actor.find({"Address.Country":"Spain"})
> addressDetail.forEach(function(item) {
... 
...         print("Actor name : ",item["First Name"]);
...         var name = item["First Name"];
...         db.Film.find({"actors.fname":name}).forEach(function(detail) {
...             print("Title of Film : ",detail["Title of Film"]);
...     });
... 
... });
Actor name :  Emma
Title of Film :  Little Fires


-----------------------------------
12.Retrieve films with actor details. 


> var actorDetails = db.Film.find({},{_id:0,"Title of Film":1,"actors.fname":1})
> 
> actorDetails.forEach(function(item) {
... 
...     print("Title if film : ",item["Title of Film"])
...     var name = item.actors
...     name.forEach(function(detail) {
...         var actorname = detail.fname
... 
...         db.Actor.find({"First Name":actorname}).forEach(function(actorField){
... 
...             print(tojson(actorField))
... 
...         });
...     });
... 
...     print("***********************************")
... });


Title if film :  Little Fires
{
	"_id" : ObjectId("6035d592febaf91510509bc6"),
	"Actor Id" : 14,
	"First Name" : "Emma",
	"Last Name" : "watson",
	"Address" : {
		"Street" : "spine",
		"City" : "Madrid",
		"State" : "Valencia",
		"Country" : "Spain",
		"Pin-code" : 991022
	},
	"Contact Details" : {
		"EmailId" : "emma@gmail.com",
		"PhoneNo" : "7789000044"
	},
	"Age" : 30
}
***********************************
Title if film :  Dil Tho Pagal hai
{
	"_id" : ObjectId("6035d592febaf91510509bc5"),
	"Actor Id" : 13,
	"First Name" : "Madhuri",
	"Last Name" : "Dixit",
	"Address" : {
		"Street" : "Bandra",
		"City" : "Mumbai",
		"State" : "Maharashtra",
		"Country" : "India",
		"Pin-code" : 411091
	},
	"Contact Details" : {
		"EmailId" : "madhu@gmail.com",
		"PhoneNo" : "7789005644"
	},
	"Age" : 50
}
***********************************
Title if film :  Rowdy Rathore
***********************************
Title if film :  Golmaal
***********************************
Title if film :  Tanhaji
***********************************
Title if film :  Titanic
***********************************
Title if film :  Endgame
{
	"_id" : ObjectId("6035d592febaf91510509bc8"),
	"Actor Id" : 16,
	"First Name" : "Chris",
	"Last Name" : "Evans",
	"Address" : {
		"Street" : "Wilshire Blvd",
		"City" : "Beverly",
		"State" : "Newyok",
		"Country" : "USA",
		"Pin-code" : 511010
	},
	"Contact Details" : {
		"EmailId" : "chris@gmail.com",
		"PhoneNo" : "8988880000"
	},
	"Age" : 52
}
***********************************
Title if film :  Far from Home
***********************************
Title if film :  Koyla
{
	"_id" : ObjectId("6035d592febaf91510509bc5"),
	"Actor Id" : 13,
	"First Name" : "Madhuri",
	"Last Name" : "Dixit",
	"Address" : {
		"Street" : "Bandra",
		"City" : "Mumbai",
		"State" : "Maharashtra",
		"Country" : "India",
		"Pin-code" : 411091
	},
	"Contact Details" : {
		"EmailId" : "madhu@gmail.com",
		"PhoneNo" : "7789005644"
	},
	"Age" : 50
}
***********************************
Title if film :  Tiger Zinda Hai
{
	"_id" : ObjectId("6035d591febaf91510509bc3"),
	"Actor Id" : 11,
	"First Name" : "Salman",
	"Last Name" : "Khan",
	"Address" : {
		"Street" : "Bandra",
		"City" : "Mumbai",
		"State" : "Maharashtra",
		"Country" : "India",
		"Pin-code" : 411010
	},
	"Contact Details" : {
		"EmailId" : "salu@gmail.com",
		"PhoneNo" : "8989785644"
	},
	"Age" : 52
}
***********************************




