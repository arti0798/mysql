ABST


CREATE TYPE STUD_NAME AS OBJECT (FNAME VARCHAR2(15),LNAME VARCHAR2(15));

CREATE TABLE STUD (RNO NUMBER(4) PRIMARY KEY, NAME STUD_NAME);

SELECT COLUMN_NAME, DATA_TYPE FROM USER_TAB_COLUMNS WHERE TABLE_NAME='STUD';

SELECT ATTR_NAME, LENGTH, ATTR_TYPE_NAME 
FROM USER_TYPE_ATTRS 
WHERE TYPE_NAME='STUD_NAME';




1. create type
2. use the user created type for table creation
3. describing a table structure
4. describing a type structure
5. Inserting values in the table



   INSERT INTO STUD VALUES(1,STUD_NAME( ‘Sagar’ , ’Joshi’ ));

   Select * from STUD;

   Select RNO,SNAME,LNAME from STUD;