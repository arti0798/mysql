 Entities | Elements
Elements for ddd.dtd
bib | book | author | editor | title | last | first | affiliation | publisher | price

<bib>	Root element

Sorry, no documentation.
<bib>'s children
Name	Cardinality
book	Any number
Element's model :

(book*)

<book>	Child of bib

Sorry, no documentation.
<book>'s children
Name	Cardinality
author	Any number
editor	Any number
price	Only one
publisher	Only one
title	Only one

<book>'s attributes
Name	Values	Default
year	 	 
Element's model :

(title, (author+ | editor+), publisher, price)

@year	Attribute of book

Sorry, no documentation.

Required

<author>	Child of book

Sorry, no documentation.
<author>'s children
Name	Cardinality
first	Only one
last	Only one
Element's model :

(last, first)

<editor>	Child of book

Sorry, no documentation.
<editor>'s children
Name	Cardinality
affiliation	Only one
first	Only one
last	Only one
Element's model :

(last, first, affiliation)

<title>	Child of book

Sorry, no documentation.

<last>	Child of editor,author

Sorry, no documentation.

<first>	Child of editor,author

Sorry, no documentation.

<affiliation>	Child of editor

Sorry, no documentation.

<publisher>	Child of book

Sorry, no documentation.

<price>	Child of book

Sorry, no documentation.

Generated with EditiX XML Editor at Thu May 06 22:31:44 IST 2021
