let $path := doc("/root/mySql/mysql/ORDBMS/xml/assignment/gradebook.xml")
let $cPath := $path/gradebook/grade/catalogs/catalog[cTitle="Automata"]
let $coursePath := $path/gradebook/grade/courses/course[cno=data($cPath/cno)]
let $enrollPath := $path/gradebook/grade/enrolls/enroll[term=data($coursePath/term)]| $path/gradebook/grade/enrolls/enroll[term="1996"]

let $studentPath := $path/gradebook/grade/students/student[sid=data($enrollPath/sid)]
return
        <student>{$studentPath/fname}</student>
