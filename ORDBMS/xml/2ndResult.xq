<?xml version="1.0" encoding="UTF-8"?>
<grammar xmlns="http://relaxng.org/ns/structure/1.0">
  <define name="bib">
    <element name="bib">
      <ref name="attlist.bib"/>
      <zeroOrMore>
        <ref name="book"/>
      </zeroOrMore>
    </element>
  </define>
  <define name="attlist.bib" combine="interleave">
    <empty/>
  </define>
  <define name="book">
    <element name="book">
      <ref name="attlist.book"/>
      <ref name="title"/>
      <choice>
        <oneOrMore>
          <ref name="author"/>
        </oneOrMore>
        <oneOrMore>
          <ref name="editor"/>
        </oneOrMore>
      </choice>
      <ref name="publisher"/>
      <ref name="price"/>
    </element>
  </define>
  <define name="attlist.book" combine="interleave">
    <attribute name="year"/>
  </define>
  <define name="author">
    <element name="author">
      <ref name="attlist.author"/>
      <ref name="last"/>
      <ref name="first"/>
    </element>
  </define>
  <define name="attlist.author" combine="interleave">
    <empty/>
  </define>
  <define name="editor">
    <element name="editor">
      <ref name="attlist.editor"/>
      <ref name="last"/>
      <ref name="first"/>
      <ref name="affiliation"/>
    </element>
  </define>
  <define name="attlist.editor" combine="interleave">
    <empty/>
  </define>
  <define name="title">
    <element name="title">
      <ref name="attlist.title"/>
      <text/>
    </element>
  </define>
  <define name="attlist.title" combine="interleave">
    <empty/>
  </define>
  <define name="last">
    <element name="last">
      <ref name="attlist.last"/>
      <text/>
    </element>
  </define>
  <define name="attlist.last" combine="interleave">
    <empty/>
  </define>
  <define name="first">
    <element name="first">
      <ref name="attlist.first"/>
      <text/>
    </element>
  </define>
  <define name="attlist.first" combine="interleave">
    <empty/>
  </define>
  <define name="affiliation">
    <element name="affiliation">
      <ref name="attlist.affiliation"/>
      <text/>
    </element>
  </define>
  <define name="attlist.affiliation" combine="interleave">
    <empty/>
  </define>
  <define name="publisher">
    <element name="publisher">
      <ref name="attlist.publisher"/>
      <text/>
    </element>
  </define>
  <define name="attlist.publisher" combine="interleave">
    <empty/>
  </define>
  <define name="price">
    <element name="price">
      <ref name="attlist.price"/>
      <text/>
    </element>
  </define>
  <define name="attlist.price" combine="interleave">
    <empty/>
  </define>
  <start>
    <choice>
      <ref name="bib"/>
    </choice>
  </start>
</grammar>
