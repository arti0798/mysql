EXTRA PART

create or replace VIEW viewT1
AS
select * from t1 where a = 1;


\! vi viewT1
MariaDB [ARTI]> \. viewT1
Query OK, 0 rows affected (0.139 sec)

MariaDB [ARTI]> select * from viewT1;
    -> //
+---+------+------+
| a | b    | c    |
+---+------+------+
| 1 | helo | NULL |
+---+------+------+
1 row in set (0.001 sec)

=================================================



